const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const { firstName, lastName, email, phoneNumber, password } = req.body;
  if (req.method !== 'POST') {
    return next();
  } else if (!firstName) {
    return res
      .status(400)
      .json({ error: true, message: 'Please enter first name' });
  } else if (!lastName) {
    return res
      .status(400)
      .json({ error: true, message: 'Please enter last name' });
  } else if (!email || email.slice(-10) !== '@gmail.com') {
    return res.status(400).json({
      error: true,
      message: 'Please enter email that ends @gmail.com',
    });
  } else if (
    !phoneNumber ||
    phoneNumber.slice(0, 4) !== '+380' ||
    phoneNumber.length !== 13
  ) {
    return res.status(400).json({
      error: true,
      message: 'Please enter phone number that starts with +380xxxxxxxxx',
    });
  } else if (!password || password.length < 3) {
    return res.status(400).json({
      error: true,
      message: 'Password must be at least 3 characters long',
    });
  }
  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  function check() {
    const result = {};
    const filter = Object.keys(req.body)
      .filter((item) => {
        const index = Object.keys(user).indexOf(item);
        return (
          item === Object.keys(user)[index] && item !== Object.keys(user)[0]
        );
      })
      .forEach((element) => {
        result[element] = req.body[element];
      });
    return result;
  }
  if (req.method !== 'PUT') {
    return next();
  } else if (Object.keys(check()).length < 1) {
    return res.status(400).json({
      error: true,
      message: 'Invalid update data',
    });
  } else {
    req.body = check();
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
