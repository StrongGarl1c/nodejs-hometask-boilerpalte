const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { name, defense, power } = req.body;
  if (req.method !== 'POST') {
    return next();
  } else if (!name) {
    return res.status(400).json({ error: true, message: 'Please enter name' });
  } else if (!power || isNaN(power) === true || power > 99) {
    return res.status(400).json({
      error: true,
      message: 'Please enter fighter power from 1 to 99',
    });
  } else if (
    !defense ||
    isNaN(defense) === true ||
    defense < 1 ||
    defense > 10
  ) {
    return res.status(400).json({
      error: true,
      message: 'Please enter fighter defense from 1 to 10',
    });
  }
  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  function check() {
    const result = {};
    Object.keys(req.body)
      .filter((item) => {
        const index = Object.keys(fighter).indexOf(item);
        return (
          item === Object.keys(fighter)[index] &&
          item !== Object.keys(fighter)[0]
        );
      })
      .forEach((element) => {
        result[element] = req.body[element];
      });
    if (
      (result.health && isNaN(result.health) === true) ||
      (result.power && isNaN(result.power) === true) ||
      (result.defense && isNaN(result.defense) === true) ||
      (result.defense && result.defense > 10) ||
      (result.defense && result.defense < 1) ||
      (result.power && result.power > 99) ||
      (result.power && result.power < 1) ||
      (result.health && result.health < 1)
    ) {
      return false;
    } else {
      return result;
    }
  }
  if (req.method !== 'PUT') {
    return next();
  } else if (Object.keys(check()).length < 1) {
    return res.status(400).json({
      error: true,
      message: 'Invalid fighter update data',
    });
  } else {
    req.body = check();
    next();
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
