const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  // TODO: Implement methods to work with fighters
  create(req) {
    return FighterRepository.create(req);
  }
  get() {
    return FighterRepository.getAll();
  }
  delete(id) {
    return FighterRepository.delete(id);
  }
  update(id, data) {
    return FighterRepository.update(id, data);
  }
  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
