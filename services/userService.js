const { UserRepository } = require('../repositories/userRepository');

class UserService {
  // TODO: Implement methods to work with user
  create(req) {
    return UserRepository.create(req);
  }
  get() {
    return UserRepository.getAll();
  }
  delete(id) {
    return UserRepository.delete(id);
  }
  update(id, data) {
    return UserRepository.update(id, data);
  }
  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
