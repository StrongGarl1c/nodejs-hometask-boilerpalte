const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid,
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.use(createFighterValid);
router.use(updateFighterValid);

router.get('/', (req, res) => {
  if (FighterService.get().length > 0) {
    res.status(200).json(FighterService.get());
  } else {
    res.status(404).json({
      error: true,
      message: `Fighters not found`,
    });
  }
});

router.get('/:id', (req, res) => {
  const obj = {
    id: req.params.id,
  };
  if (FighterService.search(obj) !== null) {
    res.status(200).json(FighterService.search(obj));
  } else {
    res.status(404).json({
      error: true,
      message: `Fighter with id: ${req.params.id} not found`,
    });
  }
});

router.post('/', (req, res, next) => {
  const obj = {
    name: req.body.name,
  };
  if (FighterService.search(obj) === null) {
    res.status(200).json(FighterService.create(req.body));
  } else {
    res.status(400).json({
      error: true,
      message: `Fighter with name: ${req.body.name} alreay exists`,
    });
  }
});

router.put('/:id', (req, res) => {
  const obj = {
    id: req.params.id,
  };
  if (FighterService.search(obj) !== null) {
    res.status(200).json(FighterService.update(req.params.id, req.body));
  } else {
    res.status(404).json({
      error: true,
      message: `Fighter with id: ${req.params.id} not found`,
    });
  }
});

router.delete('/:id', (req, res) => {
  const obj = {
    id: req.params.id,
  };
  if (FighterService.search(obj) !== null) {
    res.status(200).json(FighterService.delete(req.params.id));
  } else {
    res.status(404).json({
      error: true,
      message: `Fighter with id: ${req.params.id} not found`,
    });
  }
});

module.exports = router;
