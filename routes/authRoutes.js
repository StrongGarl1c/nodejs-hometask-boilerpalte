const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/login',
  (req, res, next) => {
    try {
      // TODO: Implement login action (get the user if it exist with entered credentials)
      res.status(200).json(AuthService.login(req.body));
    } catch (err) {
      res.status(400).json({
        error: true,
        message: 'Invalid email or password',
      });
    } finally {
      next();
    }
  },
  responseMiddleware,
);

module.exports = router;
