const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.use(createUserValid);
router.use(updateUserValid);
// router.use(responseMiddleware);

router.get('/', (req, res) => {
  if (UserService.get().length > 0) {
    res.status(200).json(UserService.get());
  } else {
    res.status(404).json({
      error: true,
      message: `Users not found`,
    });
  }
});

router.get('/:id', (req, res) => {
  const obj = {
    id: req.params.id,
  };
  if (UserService.search(obj) !== null) {
    res.status(200).json(UserService.search(obj));
  } else {
    res.status(404).json({
      error: true,
      message: `User with id: ${req.params.id} not found`,
    });
  }
});

router.post('/', (req, res, next) => {
  const obj = {
    email: req.body.email,
  };
  if (UserService.search(obj) === null) {
    res.status(200).json(UserService.create(req.body));
  } else {
    res.status(400).json({
      error: true,
      message: `User with email: ${req.body.email} alreay exists`,
    });
  }
});

router.put('/:id', (req, res) => {
  const obj = {
    id: req.params.id,
  };
  if (UserService.search(obj) !== null) {
    res.status(200).json(UserService.update(req.params.id, req.body));
  } else {
    res.status(404).json({
      error: true,
      message: `User with id: ${req.params.id} not found`,
    });
  }
});

router.delete('/:id', (req, res) => {
  const obj = {
    id: req.params.id,
  };
  if (UserService.search(obj) !== null) {
    res.status(200).json(UserService.delete(req.params.id));
  } else {
    res.status(404).json({
      error: true,
      message: `User with id: ${req.params.id} not found`,
    });
  }
});

module.exports = router;
